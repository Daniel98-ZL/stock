<?php
$hostname = "localhost";
$username = "Daniel";
$password = "admin";
$database = "stock";
$row_limit = 8;
$sgbd = 'mysql'; // mysql, pgsql

// conexion to mysql
try {
    $pdo = new PDO($sgbd.":host=$hostname;dbname=$database", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "CONECTADO ☺";
} catch (PDOException $err) {
    echo "Error de conexión";
    die("Error! " . $err->getMessage());
}
